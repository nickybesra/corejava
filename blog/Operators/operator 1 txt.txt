Operators
1.Increment and Decrement ( ++ , -- )
 We can apply increment and decrement operators only for variables but not for constant values. If we trying to to apply for constant values then we will get compile time error.
ex:-	int x=10;			|			int x=10;
	int y=++x;			|			int y=++10;
	s.o.pln(y);			|			S.o.pln(y);
  -------------------------------- 				-------------------------------
                (VALID)						 (INVALID)
							        unexpected type
							        find: value
							        required: variable
RULES
⦁	Listing of increment and decrement operators are not allowed.
⦁	For final variables we can't apply increment and decrement operestors.
⦁	we can apply increment and decrement operators for every primitive type except boolean.
⦁	But in the case of increment and decrement internal type will performed auattomaticlly.
2.Arithmatic Operators  ( '+' , '-' , '*' , '/' , '%' )
If we apply any arithmatic operators between two variables "a" and "'b" then the result type is always : max(int,type of a,type of b)
ex :-	S.o.pln('a'+'b');      # 97+98
------------------------------------------------
	0/P :- 195

	S.o.pln('a'+0.89);   # 97+0.89
-------------------------------------------------
	0/P :- 97.89
Infinity:-
-> In integral arithmatic (byte,short,int,long) their is no way to representn infinity , Hence if ifnity is the result we will get arithmatic expension in integral arithmatic.
ex:-	s.o.pln(10/0);
	O/P :-runtimeexceptionn:arithmaticexpension/by zero
->But in floating point arithmatic (float,double) their is a way to represent infinity, For these float and double classes the following 2 constant :
I.	 POSITIVE_INFINITY
II.	NEGETIVE_INFINITY
->Hence even though result is infinity we want get any arithmaticexceptioninfloating point arithmatic.
ex:- s.o.pln(10/0.0);	#infinity
        s.o.pln(-10/0.0);	#-infinity
NaN (Not a Number):-
->In integral arithmatic (byte,short,int,long) their is no way to representun defined result , Hence the result is undefined we will get runtime exceptio saying arithmatic exception
ex:-	s.o.pln(0/0);
	O/P :-runtimeexceptionn:arithmaticexpension/by zero
->But in floating point arithmatic (float,double) their is a way to represent undefined results for these float and double contains "NaN" CONSTANT.
->Hence the result is undefined we want any arithmatic exception in floating point arithmatic.
ex:-	S.o.pln(0.0/0);	#NaN
	S.o.pln(-0.0/0);	#NaN
Arithmatic Excepntion
1.	It is a reuntime exception but not compile time exception.
2.	It is only possible in integer arithmatic.
3.	the only operator which cause arithmatic exception are '/' and '%'.
3.String concartenation operators (+):-
The only overloaded operator in JAVA is '+' operator . Sometimes it acts as arithmatic addition operator and sometimes it acts as string concartination operators.
RULES
⦁	If atlesst one argument is string type then it acts as concartination operator and if both arguments are number type then it acts as arithmatic addition operators.
ex:-	string a="devil";
	int b=10,c=20d=30;
	S.o.pln(a+b+c+d);
	S.o.pln(b+c+d+a);
	S.o.pln(b+a+c+d);
-----------------------------------------
	O/P :-devil102030
	           60devil
	           10devil2030
4.Relational Operators (<,<=,>,>=):-
We can apply relational operators for every primitive type except boolean.
ex:-	S.o.pln(10<20);		#true
	S.o.pln('a'<10);		#false
	S.o.pln('a'<97.6);	#true
	S.o.pln('a'>'A');		#true
	S.o.pln(true>false);	#operator > cannot be applied to boolesan , boolean
We can't apply relational operators for object types.
ex:-	S.o.pln('devil123'>'devil');	#operator > cannot be applied to j.l.string , j.l.string
5.Equality Operators (== , !==):-
We can apply equality operators for every primitive type including boolan type also.
ex:-	S.o.pln(10==20);		#false
	S.o.pln('a'=='b');			#false
	S.o.pln('a'==97.0);		#true
	S.o.pln(false==false);		#true
We can apply equality operators for object type also.For objects reference r1,r2 
	r1==r2 (true) if and only if both references pointing to the same object (Reference 
		           comparision are address comparision)
ex:-	Thread t1=newThread();
	Thread t2=newThread();
	Thread t3=t1;
	S.o.pln(t1==t2);		#true
	S.o.pln(t1==t3);		#true
-> If we apply equality operators for object types then cumpulsary their should some relation between argument types (either child to parent or parent to child or same type) otherwise we will get compile time error saying incomparable type.
ex:-	Thread t=newThread();
	Object o=newObject();
	String s=newString("devil");
	S.o.pln(t==o);		#false
	S.o.pln(o==s);		#false
	S.o.pln(s==t);		#incomparable type j.l.string and j.l.thread
Q.What is the defference between ' == ' & ' equal() ' ?
A.In general we can use doble equal operator(==) for reference comparision (address comparision) and ' equal() ' method for content comparision.
ex:-	String s1=new String("devil");
	String s2=new String("devil");
	S.o.pln(s1==s2);		#false
	S.o.pln(s1.equal(s2));	#true
note:
For any object reference are,r==null is always false ,But null==null is always true.
ex:-	String s=new String("devil");
	S.o.pln(s==null);	#false

	String s=null;
	S.o.pln(s==null);	#true

	S.o.pln(null==null);	#true	